from django.db import models
from .usuario import Usuario

class Cita(models.Model):
    id = models.AutoField(primary_key=True)
    usuario = models.ForeignKey(Usuario, related_name='cita', on_delete=models.CASCADE)
    ubicacion = models.CharField('Ubicacion', max_length=256)
    fecha = models.DateTimeField()
    dosis = models.CharField('Dosis', max_length=8)
    tipo = models.CharField('Tipo', max_length=20)
    