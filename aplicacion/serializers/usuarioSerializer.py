from rest_framework import serializers
from aplicacion.models.usuario import Usuario
from aplicacion.models.cita import Cita
from aplicacion.serializers.citaSerializer import CitaSerializer

class UsuarioSerializer(serializers.ModelSerializer):
    cita = CitaSerializer()
    class Meta:
        model = Usuario
        fields = ['cedula', 'username', 'password', 'nombres', 'apellidos', 'email', 'direccion', 'telefono' , 'cita']
    
    
    def create(self, validated_data):
        dateData = validated_data.pop('cita')
        userInstance = Usuario.objects.create(**validated_data)
        Cita.objects.create(usuario=userInstance, **dateData)
        return userInstance
    
    def to_representation(self, obj):
        usuario = Usuario.objects.get(cedula=obj.cedula)
        cita = Cita.objects.get(usuario=obj.cedula)
        return {
            'cedula': usuario.cedula,
            'username': usuario.username,
            'nombres': usuario.nombres,
            'apellidos': usuario.apellidos,
            'email': usuario.email,
            'direccion': usuario.direccion,
            'telefono': usuario.telefono,
            'cita': {
                'id': cita.id,
                'ubicacion': cita.ubicacion,
                'fecha': cita.fecha,
                'dosis': cita.dosis,
                'tipo': cita.tipo
            }
        }

class UsuarioSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ['cedula', 'username', 'password', 'nombres', 'apellidos', 'email', 'direccion', 'telefono']

    def create(self, validated_data):
        userInstance = Usuario.objects.create(**validated_data)
        return userInstance
    
    def to_representation(self, obj):
        usuario = Usuario.objects.get(cedula=obj.cedula)
        return {
            'cedula': usuario.cedula,
            'username': usuario.username,
            'nombres': usuario.nombres,
            'apellidos': usuario.apellidos,
            'email': usuario.email,
            'direccion': usuario.direccion,
            'telefono': usuario.telefono 
        }
