from aplicacion.models.cita import Cita
from rest_framework import serializers

class CitaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cita
        fields = ['ubicacion', 'fecha', 'dosis', 'tipo']

class CitaSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cita
        fields = ['usuario', 'ubicacion', 'fecha', 'dosis', 'tipo']