from django.contrib import admin
from .models.usuario import Usuario
from .models.cita import Cita

admin.site.register(Usuario)
admin.site.register(Cita)
