from rest_framework import views, status
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from aplicacion.models import Usuario
from aplicacion.serializers.usuarioSerializer import UsuarioSimpleSerializer
from rest_framework import generics

class UsuarioUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Usuario.objects.all()
    serializer_class = UsuarioSimpleSerializer