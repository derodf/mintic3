from aplicacion.models import Cita
from aplicacion.serializers.citaSerializer import CitaSimpleSerializer
from rest_framework import generics


class CitaCreateView(generics.ListCreateAPIView):

    queryset = Cita.objects.all() 
    serializer_class = CitaSimpleSerializer

class CitaUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Cita.objects.all()
    serializer_class = CitaSimpleSerializer

